package magicgoose.greenboxauth.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.gson.Gson;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceUser;

import magicgoose.common.Either;
import magicgoose.common.android.rx.ReactiveActivity;
import magicgoose.greenboxauth.GoogleAzureAuthUtil;

public abstract class AzureSignInActivity extends ReactiveActivity {

    private GoogleAzureAuthUtil azureAuthUtil;

    private final static String MobileUserKey = "MobileUserKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        azureAuthUtil = getAzureAuthUtil();
        azureAuthUtil.attachActivity(this);
        subscribeUntilDestroy(azureAuthUtil.loginResult, this::saveUserAndNotify);

        final MobileServiceUser savedUser = loadUser();
        if (savedUser != null) {
            azureAuthUtil.getAzureServiceClient().setCurrentUser(savedUser);
            loginFinished(Either.createRight(savedUser));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        azureAuthUtil.deleteActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!azureAuthUtil.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    protected void tryLogin() {
        azureAuthUtil.tryLogin();
    }

    private void saveUserAndNotify(Either<Throwable, MobileServiceUser> result) {
        if (result.isRight) {
            saveUser(result.right);
        }
        loginFinished(result);
    }

    private void saveUser(MobileServiceUser user) {
        final String userSerialized = new Gson().toJson(user);
        getPreferences(MODE_PRIVATE).edit()
                .putString(MobileUserKey, userSerialized)
                .apply();
    }

    private MobileServiceUser loadUser() {

        final SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        final String savedUser = preferences.getString(MobileUserKey, null);

        if (savedUser == null)
            return null;

        return new Gson().fromJson(savedUser, MobileServiceUser.class);
    }

    protected abstract void loginFinished(Either<Throwable, MobileServiceUser> result);

    protected abstract GoogleAzureAuthUtil getAzureAuthUtil();
}
