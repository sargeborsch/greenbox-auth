package magicgoose.greenboxauth;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.JsonObject;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceAuthenticationProvider;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceUser;

import java.net.MalformedURLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import magicgoose.common.Either;
import magicgoose.common.android.SharedHandler;
import rx.Observable;
import rx.functions.Action1;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

public class GoogleAzureAuthUtil {

    private static final int REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 1002;
    private static final int REQUEST_CODE_PICK_ACCOUNT = 1000;


    private final PublishSubject<Either<Throwable, MobileServiceUser>> loginResultSubject = PublishSubject.create();
    public final Observable<Either<Throwable, MobileServiceUser>> loginResult = loginResultSubject;

    private final BehaviorSubject<Boolean> busySubject = BehaviorSubject.create(false);
    public final Observable<Boolean> isBusyObservable = busySubject.distinctUntilChanged();

    protected final ExecutorService backgroundExecutor;

    private final String scopes;
    private final Context context;

    private MobileServiceClient azureServiceClient;
    private Activity activity;
    protected volatile boolean isBusy;

    private String accountName; // the selected email

    public MobileServiceClient getAzureServiceClient() {
        return azureServiceClient;
    }


    @SuppressWarnings("SameParameterValue")
    public GoogleAzureAuthUtil(String azureAppUrl, String azureAppKey, String scopes, Context context) {

        this.scopes = scopes;
        this.context = context;

        try {
            azureServiceClient = new MobileServiceClient(azureAppUrl, azureAppKey, this.context);
        } catch (MalformedURLException e) {
            throw new Error(e);
        }

        backgroundExecutor = Executors.newFixedThreadPool(1);
    }


    public void attachActivity(Activity activity) {
        if (this.activity != null)
            throw new IllegalStateException("Only one activity can perform login at a time");

        this.activity = activity;
    }

    public void deleteActivity() {
        this.activity = null;
    }

    private void resultOK(MobileServiceUser user) {
        setBusy(false);
        loginResultSubject.onNext(Either.createRight(user));
    }

    protected void resultFail(Throwable t) {
        setBusy(false);
        loginResultSubject.onNext(Either.createLeft(t));
    }

    private void resultFail() {
        resultFail(new Exception());
    }


    public void tryLogin() {
        if (isBusy)
            return;

        if (activity == null)
            throw new Error("Attach an activity first");

        setBusy(true);

        final int isAvailableResult = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        switch (isAvailableResult) {
            case ConnectionResult.SUCCESS:
                if (this.accountName != null) {
                    accountPicked();
                } else {
                    pickUserAccount();
                }
                break;
            default:
                final String errorCause = "Google play services is not available: " + isAvailableResult;
                Log.e("error", errorCause);
                resultFail(new Exception(errorCause));
                break;
        }
    }

    protected void setBusy(boolean isBusy) {
        this.isBusy = isBusy;
        busySubject.onNext(isBusy);
    }

    private void pickUserAccount() {
        final String[] accountTypes = new String[] { "com.google" };
        final Intent intent = AccountPicker.newChooseAccountIntent(
                null, null,
                accountTypes, false,
                null, null, null, null);

        activity.startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }

    protected void accountPicked() {
        getTokenAndLogin(this.accountName);
    }

    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_PICK_ACCOUNT:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        this.accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                        accountPicked();
                        break;
                    case Activity.RESULT_CANCELED:
                        final String cause = "Activity cancelled by user";
                        resultFail(new Exception(cause));
                        break;
                    default:
                        resultFail();
                        break;
                }
                return true;
            case REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR:
                setBusy(false);
                tryLogin();
                return true;
            default:
                return false;
        }
    }


    private boolean isDeviceOnline() {
        ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = mgr.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private void getTokenAndLogin(String accountName) {
        if (isDeviceOnline()) {
            getTokenAndLoginInBackground(context, scopes, accountName);
        } else {
            resultFail();
        }
    }

    protected void getTokenAndLoginInBackground(Context context, String scopes, String email) {
        getGoogleToken(context, scopes, email, this::loginToMobileService);
    }

    protected void getGoogleToken(Context context, String scopes, String email, Action1<String> onSuccess) {
        backgroundExecutor.submit(() -> {
            try {
                final String token = fetchIdToken(context, email, scopes);
                if (token != null) {
                    onSuccess.call(token);
                }
            } catch (Exception e) {
                Log.e("error", "Exception: " + e);
                resultFail(e);
            }
        });
    }

    private void loginToMobileService(final String idToken) {
        final JsonObject loginBody = new JsonObject();
        loginBody.addProperty("id_token", idToken);

        final ListenableFuture<MobileServiceUser> loginResult =
                azureServiceClient.login(
                        MobileServiceAuthenticationProvider.Google,
                        loginBody);

        Futures.addCallback(loginResult, new FutureCallback<MobileServiceUser>() {
            @Override
            public void onSuccess(MobileServiceUser user) {
                onAzureLoginSuccess(user);
            }

            @Override
            public void onFailure(@NonNull Throwable error) {
                Log.e("error", "Login error: " + error);
                resultFail(error);
            }
        });
    }

    protected void onAzureLoginSuccess(final MobileServiceUser user) {
        resultOK(user);
    }

    private String fetchIdToken(Context context, String email, String scopes) throws Exception {
        try {
            return GoogleAuthUtil.getToken(context, email, scopes);
        } catch (UserRecoverableAuthException e) {
            handleRecoverableException(e, activity);
        } catch (GoogleAuthException gae) {
            Log.e("error", "Unrecoverable exception: " + gae);
            resultFail(gae);
        }
        return null;
    }

    private void handleRecoverableException(final Exception e, final Activity activity) {

        if (activity == null)
            return;

        SharedHandler.post(() -> {
            if (e instanceof GooglePlayServicesAvailabilityException) {
                // The Google Play services APK is old, disabled, or not present.
                // Show a dialog created by Google Play services that allows
                // the user to update the APK
                int statusCode = ((GooglePlayServicesAvailabilityException)e)
                        .getConnectionStatusCode();
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(statusCode,
                        activity,
                        REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                dialog.show();
            } else if (e instanceof UserRecoverableAuthException) {
                // Unable to authenticate, such as when the user has not yet granted
                // the app access to the account, but the user can fix this.
                // Forward the user to an activity in Google Play services.
                Intent intent = ((UserRecoverableAuthException)e).getIntent();
                activity.startActivityForResult(intent,
                        REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
            }
        });
    }
}
