package magicgoose.greenboxauth;

import android.content.Context;
import android.util.Pair;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.JsonElement;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceUser;

import java.util.Arrays;

public class GoogleAzureAuthUtilTwoPass extends GoogleAzureAuthUtil {

    private final String extraScopes;
    private String extraToken;

    public GoogleAzureAuthUtilTwoPass(final String azureAppUrl, final String azureAppKey, final String scopes, final String extraScopes, final Context context) {
        super(azureAppUrl, azureAppKey, scopes, context);
        this.extraScopes = extraScopes;
    }

    @Override
    protected void getTokenAndLoginInBackground(final Context context, final String scopes, final String email) {
        if (extraToken != null) {
            super.getTokenAndLoginInBackground(context, scopes, email);
        } else {
            getGoogleToken(context, extraScopes, email, this::gotFirstToken);
        }
    }

    private void gotFirstToken(final String token) {
        this.extraToken = token;
        setBusy(false);
        tryLogin();
    }

    @Override
    protected void onAzureLoginSuccess(final MobileServiceUser user) {
        final ListenableFuture<JsonElement> loginCallResult = this.getAzureServiceClient().invokeApi("login", "POST",
                Arrays.asList(Pair.create("access_token", this.extraToken)));

        Futures.addCallback(loginCallResult, new FutureCallback<JsonElement>() {
            @Override
            public void onSuccess(final JsonElement jsonElement) {
                GoogleAzureAuthUtilTwoPass.super.onAzureLoginSuccess(user);
            }

            @Override
            public void onFailure(final Throwable err) {
                resultFail(err);
            }
        });
    }
}
